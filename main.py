import yaml
import asyncio
import aiohttp
from pathlib import Path

from pprint import pprint

TOKEN_FILE = Path.cwd() / "token"


async def get_token_with_secret(config, additional_scopes=[]):
    """request token on behalf of app.  requires admin consent"""
    tenant_id, client_id, client_secret = [
        config[k] for k in ["tenant_id", "client_id", "client_secret"]
    ]
    scopes = ["https://graph.microsoft.com/.default", *additional_scopes]
    async with aiohttp.ClientSession() as session:
        payload = {
            "client_id": client_id,
            "scope": " ".join(scopes),
            "client_secret": client_secret,
            "grant_type": "client_credentials",
        }
        res = await session.post(
            f"https://login.microsoftonline.com/{tenant_id}/oauth2/v2.0/token",
            data=payload,
        )
        assert res.status == 200, "unexpected response code! ({res.status})"
        body = await res.json()

        assert "access_token" in body, "missing token in response payload"
        token = body["access_token"]
        return token


async def get_token_with_device_code(config, additional_scopes=[]):
    """request token on behalf of user with device code"""
    tenant_id, client_id, username, password = [
        config[k] for k in ["tenant_id", "client_id", "username", "password"]
    ]

    scopes = [
        "offline_access",  # refresh_token
        "openid",
        "https://graph.microsoft.com/.default",
        *additional_scopes,
    ]
    async with aiohttp.ClientSession() as session:
        payload = {
            "client_id": client_id,
            "scope": " ".join(scopes),
            "username": username,
            "password": password,
            "grant_type": "password",
        }
        res = await session.post(
            f"https://login.microsoftonline.com/{tenant_id}/oauth2/v2.0/devicecode",
            data=payload,
        )
        try:
            assert res.status == 200, f"unexpected response code! ({res.status})"
        except AssertionError as e:
            pprint(await res.json())
            raise e
        body = await res.json()

        assert "message" in body, "missing token in response payload"
        message = body["message"]
        device_code = body["device_code"]

        print(message)

        payload = {
            "grant_type": "urn:ietf:params:oauth:grant-type:device_code",
            "client_id": client_id,
            "device_code": device_code,
        }

        wait_interval = 4
        wait_for = 40
        token = None
        for i in range(int(wait_for / wait_interval)):
            await asyncio.sleep(wait_interval)
            res = await session.post(
                f"https://login.microsoftonline.com/{tenant_id}/oauth2/v2.0/token",
                data=payload,
            )
            if res.status == 200:
                body = await res.json()
                assert "access_token" in body, "missing token in response payload"
                token = body["access_token"]
                break
        else:
            raise Exception("failed to retrieve token after {wait_for} seconds")

        return token


async def get_token_with_credentials(config, additional_scopes=[]):
    """
    request token on behalf of user with username / password.  will not work
    if user has MFA enabled
    """
    tenant_id, client_id, username, password = [
        config[k] for k in ["tenant_id", "client_id", "username", "password"]
    ]

    scopes = [
        "offline_access",  # refresh_token
        "openid" "https://graph.microsoft.com/.default",
        *additional_scopes,
    ]
    async with aiohttp.ClientSession() as session:
        payload = {
            "client_id": client_id,
            "scope": " ".join(scopes),
            "username": username,
            "password": password,
            "grant_type": "password",
        }
        res = await session.post(
            f"https://login.microsoftonline.com/{tenant_id}/oauth2/v2.0/token",
            data=payload,
        )
        try:
            assert res.status == 200, f"unexpected response code! ({res.status})"
        except AssertionError as e:
            pprint(await res.json())
            raise e
        body = await res.json()

        assert "access_token" in body, "missing token in response payload"
        token = body["access_token"]
        return token


async def get_token(config):
    if TOKEN_FILE.is_file():
        with open(TOKEN_FILE, "r") as f:
            token = f.read()
        return token

    token = await get_token_with_device_code(config)
    # token = await get_token_with_credentials(config)
    # token = await get_token_with_secret(config)

    with open(TOKEN_FILE, "w") as f:
        f.write(token)

    return token


async def main(config):
    token = await get_token(config)

    headers = {"Authorization": f"Bearer {token}"}
    async with aiohttp.ClientSession(headers=headers) as session:
        origin = "https://graph.microsoft.com"
        version = "v1.0"
        path = "users"
        # path = "me"
        res = await session.get(f"{origin}/{version}/{path}", headers=headers)
        body = await res.json()
        pprint(body)


if __name__ == "__main__":
    with open("config.yaml") as f:
        config = yaml.safe_load(f)
    asyncio.run(main(config))
